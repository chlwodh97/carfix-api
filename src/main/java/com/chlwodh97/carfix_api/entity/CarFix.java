package com.chlwodh97.carfix_api.entity;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class CarFix {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private LocalDate fixDate;

    @Column(nullable = false)
    private String fixerName;

    @Column(nullable = false)
    private String car;

    @Column(nullable = false)
    private Double price;

    @Column(nullable = false)
    private String fixContent;

    @Column(columnDefinition = "TEXT")
    private String etcMemo;
}
