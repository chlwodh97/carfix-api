package com.chlwodh97.carfix_api.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;


@Getter
@Setter
public class CarRequest {
    private LocalDate fixDate;
    private String fixerName;
    private String car;
    private Double price;
    private String fixContent;
    private String etcMemo;
}
