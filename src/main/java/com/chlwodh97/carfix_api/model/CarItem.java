package com.chlwodh97.carfix_api.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;


@Getter
@Setter
public class CarItem {
    private Long id;
    private LocalDate fixDate;
    private String fixerName;
    private String car;
}
