package com.chlwodh97.carfix_api.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FixerNameRequest {
    private String fixerName;
}
