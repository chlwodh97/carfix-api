package com.chlwodh97.carfix_api.service;

import com.chlwodh97.carfix_api.entity.CarFix;
import com.chlwodh97.carfix_api.model.CarItem;
import com.chlwodh97.carfix_api.model.CarRequest;
import com.chlwodh97.carfix_api.model.CarResponse;
import com.chlwodh97.carfix_api.model.FixerNameRequest;
import com.chlwodh97.carfix_api.repository.CarRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CarService {
    private final CarRepository carRepository;

    public void setCarFix(CarRequest request){

        CarFix addData = new CarFix();
        addData.setFixDate(request.getFixDate());
        addData.setFixerName(request.getFixerName());
        addData.setCar(request.getCar());
        addData.setPrice(request.getPrice());
        addData.setFixContent(request.getFixContent());
        addData.setEtcMemo(request.getEtcMemo());

        carRepository.save(addData);
    }


    public List<CarItem> getCarFixs() {

        List<CarFix> originList = carRepository.findAll();

        List<CarItem> result = new LinkedList<>();

        for (CarFix carFix : originList ){
            CarItem addItem = new CarItem();
            addItem.setId(carFix.getId());
            addItem.setFixDate(carFix.getFixDate());
            addItem.setFixerName(carFix.getFixerName());
            addItem.setCar(carFix.getCar());

            result.add(addItem);

        }
            return result;
    }

    public CarResponse getCarFix(long id) {
        // 리턴 타입은 카리스폰 받을거임 이름은 getCarFix (long 타입 id 필요)

        CarFix originData = carRepository.findById(id).orElseThrow();
        //엔티티 카픽스의 이름 originData 는 레포지토리에서 아이디를 꺼내옴

        CarResponse response = new CarResponse();
        //모델에 카리스폰의 이름 reponse 는 새로운(비어있는) CarResponse 야
        response.setId(originData.getId());
        //reponse안에fixDate셋팅할거야(오리진 데이터 안에있는 FixDate 를 가져와서)
        response.setFixDate(originData.getFixDate());
        response.setFixerName(originData.getFixerName());
        response.setCar(originData.getCar());
        response.setPrice(originData.getPrice());
        response.setFixContent(originData.getFixContent());
        response.setEtcMemo(originData.getEtcMemo());

        return response;
        // 윗 값이 다 들어간 리스폰스를 돌려줄게



    }




    }


