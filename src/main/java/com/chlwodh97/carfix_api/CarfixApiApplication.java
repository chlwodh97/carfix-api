package com.chlwodh97.carfix_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarfixApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(CarfixApiApplication.class, args);
    }

}
