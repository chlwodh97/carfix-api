package com.chlwodh97.carfix_api.controller;


import com.chlwodh97.carfix_api.entity.CarFix;
import com.chlwodh97.carfix_api.model.CarItem;
import com.chlwodh97.carfix_api.model.CarRequest;
import com.chlwodh97.carfix_api.model.CarResponse;
import com.chlwodh97.carfix_api.model.FixerNameRequest;
import com.chlwodh97.carfix_api.repository.CarRepository;
import com.chlwodh97.carfix_api.service.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/car")
public class CarController {
    private final CarService carService;


    @PostMapping("/accident")
    public String setCarFix(@RequestBody CarRequest request){
        carService.setCarFix(request);

        return "무사고 기원";
    }

    @GetMapping("/all")
    public List<CarItem> getCarFixs() {

        return carService.getCarFixs();
    }

    @GetMapping("/detail/{id}")
    public CarResponse getCarfix(@PathVariable long id){
        return carService.getCarFix(id);
    }

    @PutMapping("/fixer-name/{id}")
    public String putFixerName(@PathVariable long id , @RequestBody FixerNameRequest request){

        carService.putFixerName(id ,request);
        return "수리자 이름 수정하였습니다.";
    }
}
