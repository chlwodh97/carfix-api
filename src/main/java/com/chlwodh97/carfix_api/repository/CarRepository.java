package com.chlwodh97.carfix_api.repository;

import com.chlwodh97.carfix_api.entity.CarFix;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarRepository extends JpaRepository <CarFix , Long> {
}
